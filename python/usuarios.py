#!/usr/bin/python3.7
# -*- coding: UTF-8 -*-
# 
# Obtiene la información de los usuarios,
# sus grupos, políticas (tanto administradas como personalizadas)
# y la fecha de activación de MFA en caso de existir
# que tiene asignadas, y las despliega en formato separado por
# tabuladores, de forma que es posible importarlo o copiarlo en
# una hoja de cálculo.
# Agradecimiento a Francisco Jaime Izquierdo por la parte del paginator

# Uso: python usuarios.py <profile de conexión AWS a utilizar, default si no se especifíca>

# 2019 David Sol

# Se requiere del módulo sys para obtener los argumentos del comando
from sys import argv, exit
# boto3 para el acceso a AWS
import boto3

# Si no se especifió el profile se elige "default" por omisión
if len(argv) == 1 :
    profile = 'default'
# Si se especificó un profile, se utiliza
elif len(argv) == 2 :
    profile = argv[1]
# Si hay mas de un parámetro se despliega ayuda, y se termina el programa.
else :
    print ('Uso: python '+ argv[0] + ' [profile, default por omisión]')
    exit(0)

# Se crea el objeto session con el profile elegido
session = boto3.Session(profile_name=profile)
# Se crea el objeto client de IAM para realizar las consultas
iam = session.client('iam')

# Obtiene la lista de usuarios, en caso de error
# se despliega el error y se termina el programa.
try :
    users = iam.get_paginator('list_users')
except Exception as e :
    print ('No se pudo obtener la lista de usuarios: ' + str(e))
    exit(0)

# Genera el iterador
users_iterator = users.paginate()

# Para cada bloque de usuarios...
for page_block in users_iterator :
    # Para cada uno de los usuarios...
    for user in page_block['Users']:
        # ... asigna el valor de userName, para utilizarse en las demás llamadas
        userName = user['UserName']
        # ... despliega el UserName
        print ('Usuario\t' + userName)
        # Obtiene la lista de grupos a la que pertenece
        List_of_Groups =  iam.list_groups_for_user(UserName=userName)
        # Para cada grupo...
        for group in List_of_Groups['Groups']:
            # ... se imprime el nombre del grupo
            print ('\tGrupo\t' + group['GroupName'])
        # Obtiene las políticas administrada (de AWS)... 
        List_of_Attached_Policies =  iam.list_attached_user_policies(UserName=userName)
        # Para cada política administrada (de AWS)...
        for attachedPolicy in List_of_Attached_Policies['AttachedPolicies']:
            # ... imprime el nombre de la política
            print ('\tPolitica\t' + attachedPolicy['PolicyName'])
        # Obtiene las políticas personalizadas (de la cuenta)... 
        List_of_Policies =  iam.list_user_policies(UserName=userName)
        # Para cada política personalizadas (de la cuenta)... 
        for Policy in List_of_Policies['PolicyNames']:
            # ... imprime el nombre de la política
            print ('\tPolitica\t' + Policy)
        # Obtiene la lista de dispositivos de autenticación de 2 factores
        List_of_MFA_Devices = iam.list_mfa_devices(UserName=userName)
        # Para cada uno de ellos
        for MFA in List_of_MFA_Devices['MFADevices']:
            # ... imprime la fecha de activación
            print ('\tMFA\t' + MFA['EnableDate'].strftime('%Y-%m-%d %H:%M'))
